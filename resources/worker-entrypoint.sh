#!/bin/bash

# export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts -Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"
# export JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts -Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"

fortifyupdate -url "${FORTIFY_UPDATE_FROM_URL:-https://update.fortify.com}" -acceptKey

if [[ ${POOL_ID} ]]; then     
    scancentral -url ${SCAN_CENTRAL_CONTROLLER_URL} worker -pool ${POOL_ID}
else
    scancentral -url ${SCAN_CENTRAL_CONTROLLER_URL} worker
fi
