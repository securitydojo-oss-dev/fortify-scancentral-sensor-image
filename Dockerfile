FROM ubuntu:20.04

ARG RESOURCE_PATH=./resources
ARG CREATED_USER=ciuser
ARG FORTIFY_VERSION=22.2.1
ARG FORTIFY_HOME=/opt/fortify

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris

# "${RESOURCE_PATH}/customcacerts", 

# Get Packages / Binaries dependencies
RUN apt-get update && apt-get install wget -y

ARG PKGREGISTRY_JOB_TOKEN
ENV TOKEN=$PKGREGISTRY_JOB_TOKEN
# ARG TOKEN_TYPE=PRIVATE-TOKEN
ARG TOKEN_TYPE=JOB-TOKEN
# ARG PKGREGISTRY_JOB_TOKEN
# ENV PKGREGISTRY_JOB_TOKEN=$PKGREGISTRY_JOB_TOKEN
RUN wget --header "${TOKEN_TYPE}:${TOKEN}" "https://gitlab.com/api/v4/projects/36631472/packages/generic/fortify-sca-and-apps/${FORTIFY_VERSION}/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run" -P "/root/"

COPY [ "${RESOURCE_PATH}/fortify.license", "${RESOURCE_PATH}/fortify.options", "${RESOURCE_PATH}/worker.properties", "${RESOURCE_PATH}/worker-entrypoint.sh", "/root/" ]

# Installation des apt et des packets utilisés
RUN adduser --disabled-password --shell /bin/bash --home /home/${CREATED_USER} ${CREATED_USER}          && \
    # Add Entrypoints
    mkdir -p /entrypoints                                                                               && \
    mv /root/*-entrypoint.sh /entrypoints                                                               && \
    chmod +x /entrypoints/*                                                                             && \
    chown ${CREATED_USER}:${CREATED_USER} -R /entrypoints                                               && \
    # Install needed tools
    apt-get update                                                                                      && \
    apt-get install -y wget curl gnupg2 ca-certificates lsb-release apt-transport-https                 \
    unzip software-properties-common                                                                    && \
    # Add needed custom repositories
    wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -            && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/                            && \
    add-apt-repository --yes ppa:ondrej/php                                                             && \
    apt-get update                                                                                      && \
    # Install OpenJDK 11
    apt-get install -y adoptopenjdk-11-hotspot                                                     
# # Certif
# RUN chown -R ${CREATED_USER} /root/customcacerts                                                        && \
# cp /root/customcacerts $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts        && \
# chmod +r $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts                      

# Install Fortify
RUN chmod u+x /root/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run
RUN /root/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run --mode unattended --optionfile /root/fortify.options

RUN cp /root/worker.properties ${FORTIFY_HOME}/Core/config/worker.properties                            && \
    rm /root/worker.properties                                                                          && \
    # Give rights and clean
    chown ${CREATED_USER}:${CREATED_USER} -R /opt/*                                                     && \
    rm -r /root/*

USER ${CREATED_USER}

# WORKDIR ${FORTIFY_HOME}/plugins/maven/maven-plugin-bin
# RUN chmod u+x ./install.sh                                                                               && \
#     ./install.sh

WORKDIR /home/${CREATED_USER}

ENV USER_HOME=/home/${CREATED_USER}                                                                      \
    CREATED_USER=${CREATED_USER}                                                                         \
    FORTIFY_VERSION=${FORTIFY_VERSION}                                                                   \
    FORTIFY_HOME=${FORTIFY_HOME}                                                                         \
    PATH=${FORTIFY_HOME}/bin:${PATH}                                                                   

ENTRYPOINT [ "/entrypoints/worker-entrypoint.sh" ]
